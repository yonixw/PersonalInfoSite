﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PersonalInfoSite.DataSets.DatasetTableAdapters;
using PersonalInfoSite.DataSets;
using System.Web.Script.Serialization;

namespace PersonalInfoSite.Pages
{
    public class DomainUser
    {
        public string Domain;
        public string Username;
        private string _myFullName = "";

        public DomainUser(string FullUsername)
        {
            string[] data = FullUsername.Split('\\');
            if (data.Length != 2)
            {
                Domain = "NO DOMAIN";
                Username = FullUsername;
            }
            else
            {
                Domain = data[0];
                Username = data[1];
            }

            _myFullName = Domain + "\\" + Username;
        }


        public override string ToString()
        {
            return _myFullName;
        }

        public static implicit operator string (DomainUser d)
        {
            return d._myFullName;
        }
    }

    public static class DbTools
    {
        public static JavaScriptSerializer json = new JavaScriptSerializer();

        public static PagesTableAdapter myPagesAdapter = new PagesTableAdapter();
        public static UserProfilesTableAdapter myPagesProfileAdapter = new UserProfilesTableAdapter();
        public static AdminUsersTableAdapter myAdminUsersAdapter = new AdminUsersTableAdapter();
        public static PeopleTitlesTableAdapter myTitleAdapter = new PeopleTitlesTableAdapter();
        public static SearchPeopleTableAdapter mySearchPeopleViewAdapter = new SearchPeopleTableAdapter();

        // This table holds the albums and who owns them:
        public static UsersAlbumTableAdapter myUserAlbums = new UsersAlbumTableAdapter();

        // This view shows all detail about photos. can query based on album.
        public static AlbumPhotosViewTableAdapter myAlbumPhotos = new AlbumPhotosViewTableAdapter();

        #region Pages

        public static int getPageIdFromOwner(string Owner)
        {
            int result = -1;

            Dataset.PagesDataTable dt = myPagesAdapter.GetIDByOwner(Owner);
            if (dt.Rows.Count > 0)
            {
                result = dt[0].PageID;
            }

            return result;
        }

        public static int CreatePage(string owner, out bool Exists)
        {
            int pageId = getPageIdFromOwner(owner);
            if (pageId > -1)
            {
                Exists = true;
                return pageId; // Already Exists!
            }

            // Create Page:
            myPagesAdapter.NewPage(owner);
            Dataset.PagesDataTable dt = myPagesAdapter.GetIDByOwner(owner);

            if (dt.Rows.Count == 0)
                throw new Exception("Can't create new page in db!");

            pageId = dt[0].PageID;

            // Create Profile:
            myPagesProfileAdapter.NewProfile(pageId);

            // Page now ready:
            Exists = false;
            return pageId;
        }

        public static int CreatePageAdmin()
        {
            string adminStamp = "A:" + MaxPageID(); // Uniqe id to find it later.

            // Create Page:
            myPagesAdapter.NewPage(adminStamp);
            Dataset.PagesDataTable dt = myPagesAdapter.GetIDByOwner(adminStamp);

            if (dt.Rows.Count == 0)
                throw new Exception("Can't create new page in db! (Admin create)");

            int pageId = dt[0].PageID;

            // Create Profile:
            myPagesProfileAdapter.NewProfile(pageId);

            // Page now ready:
            return pageId;
        }

        public static int MaxPageID()
        {
            return (myPagesAdapter.MaxID() ?? 1);
        }

        #endregion


        #region AdminUser

        public static bool isAdmin(string UserName)
        {
            return (myAdminUsersAdapter.isAdmin(UserName) > 0);
        }

        public static void addAdmin(string UserName)
        {
            if (!isAdmin(UserName))
                myAdminUsersAdapter.NewAdminUser(UserName);
        }

        public static void deleteAdmin(string UserName)
        {
            if (isAdmin(UserName))
                myAdminUsersAdapter.DeleteAdmin(UserName);
        }

        #endregion

        #region PageEdit

        public static Dataset.UserProfilesRow getProfile(int pageID)
        {
            Dataset.UserProfilesDataTable dt = myPagesProfileAdapter.GetDataByPageID(pageID);
            if (dt.Rows.Count == 0) return null;
            return dt[0];
        }

        public static Dataset.PeopleTitlesDataTable getPeopleTitles()
        {
            return myTitleAdapter.GetData();
        }

        #endregion


        #region Photo Galleries




        #endregion
    }
}