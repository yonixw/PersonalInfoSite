﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Try.aspx.cs" Inherits="PersonalInfoSite.Pages.Try" %>

<asp:Content ID="TryContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <%=DateTime.Now.ToString()%> <br />
        <%=User.Identity.Name %> <br />
        

        <asp:Label ID="lblCount" runat="server" Text="Label"></asp:Label>
        &nbsp;<asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="+" />

    </div>
    <div>

        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        <asp:Button ID="btnSayName" runat="server" OnClick="btnSayName_Click" Text="Say!" />
        <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>

    </div>
    <div id="db">
        <asp:Label ID="lblPageExist" runat="server" Text="Label"></asp:Label>
        <asp:Button ID="btnAddPage" runat="server" Text="Create Page" OnClick="btnAddPage_Click" />
        <asp:Button ID="btnDeletePage" runat="server" Text="Unlink Page" OnClick="btnDeletePage_Click" />
    </div>
</asp:Content>
