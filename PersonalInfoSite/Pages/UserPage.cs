﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalInfoSite.Pages
{
    public class UserPage :  System.Web.UI.Page
    {
        protected DomainUser MyUser;

        public UserPage() : base()
        {
            MyUser = new DomainUser(User.Identity.Name);
        }
    }
}