﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PersonalInfoSite.DataSets.DatasetTableAdapters;
using PersonalInfoSite.DataSets;

namespace PersonalInfoSite.Pages
{
    public partial class Try : System.Web.UI.Page
    {

        const string myCounterTag = "count";

        public Try()
        {

        }




        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Session[myCounterTag] = 0;

            lblName.Text = User.Identity.Name;
            lblCount.Text = "Clicked: " + Session[myCounterTag];

            lblPageExist.Text = "Your Id: " + DbTools.getPageIdFromOwner(User.Identity.Name);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Session[myCounterTag] = (int)Session[myCounterTag] + 1;
            lblCount.Text = "Clicked: " + Session[myCounterTag];
        }

        protected void btnSayName_Click(object sender, EventArgs e)
        {
            lblName.Text = "Your name: " + txtName.Text;


        }

        protected void btnAddPage_Click(object sender, EventArgs e)
        {
            if (DbTools.getPageIdFromOwner(User.Identity.Name) < 0 && User.Identity.IsAuthenticated)
            {
                //myPagesAdapter.NewPage(User.Identity.Name);
                lblPageExist.Text = "Your Id: " + DbTools.getPageIdFromOwner(User.Identity.Name);
            }
        }

        protected void btnDeletePage_Click(object sender, EventArgs e)
        {
            int myPageIndex = DbTools.getPageIdFromOwner(User.Identity.Name);
            if (myPageIndex >= 0 && User.Identity.IsAuthenticated)
            {
                //myPagesAdapter.UnlinkOwner(myPageIndex);
                lblPageExist.Text = "Your Id: " + DbTools.getPageIdFromOwner(User.Identity.Name);
            }

            // Do not use: 
            //lblName.Text = UserPrincipal.Current.DisplayName; (Use PROCESS info and not user)
        }
    }
}