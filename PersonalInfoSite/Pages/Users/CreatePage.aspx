﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreatePage.aspx.cs" MasterPageFile="~/Site.Master" Inherits="PersonalInfoSite.Pages.Users.CreatePage" %>

<asp:Content ID="CreatePageContent" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <asp:Panel ID="pnlNewUser" runat="server" Visible="false">
            ברוך הבא למערכת,<br />
            הדף האישי שלך נוצר בהצלחה!<br />
        </asp:Panel>
        <asp:Panel ID="pnlExistingUser" runat="server" Visible="false">
            ברוך השב למערכת,<br />
            הדף האישי שלך מוכן לעריכה.<br />
        </asp:Panel>
        כדי להגיע לדף האישי, לחץ "דף אישי" בתפריט העליון.
    </p>
    <asp:Panel ID="pnlAdmin" runat="server" Visible="false">
        <p>
            אתה מנהל במערכת זאת וביכולתך לערוך כל דף במערכת.
            <br />
            <asp:LinkButton ID="btnAdminZone" runat="server" Text="עבור לדף מנהל" OnClick="btnAdminZone_Click" />
            <br />
            <br />
            בנוסף, ביכולתך ליצור דפים ללא הגבלה: 
            <br />
            <asp:Button ID="btnCreateAdmin" runat="server" Text="צור דף חדש ועבור לעריכה" OnClick="btnCreateAdmin_Click" />
        </p>
    </asp:Panel>
    <br />


</asp:Content>
