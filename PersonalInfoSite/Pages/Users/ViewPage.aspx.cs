﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PersonalInfoSite.DataSets;

namespace PersonalInfoSite.Pages.Users
{
    public partial class ViewPage : UserPage
    {

        void loadProfileImage()
        {
            imgMyProfile.ImageUrl = Picture1.pictureService(Picture1.PictureType.UserProfile, pageID.ToString());
        }

        int pageID = -1; // Calculate at load;
        public bool canEdit = false; // until proven otherwise.
        string jsonBiographyPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (int.TryParse(Request.QueryString["ID"], out pageID))
            {
                canEdit = DbTools.getPageIdFromOwner(MyUser) == pageID || DbTools.isAdmin(MyUser);
                // TODO: blacklist from editing ...    

                jsonBiographyPath = Server.MapPath("~/UploadData/Biography/" + pageID + ".json.txt");
                string jsonBiographyDefault = Server.MapPath("~/UploadData/Biography/default.json.txt");

                if (!IsPostBack)
                {


                    // Load Profile data:
                    Dataset.UserProfilesRow profileRow = DbTools.getProfile(pageID);
                    if (profileRow != null)
                    {
                        // What we can edit\see
                        pnlValidID.Visible = true;
                        pnlEdit.Visible = canEdit;
                        pnlUploadProfilePic.Visible = canEdit;
                        btnSaveBiography.Visible = canEdit;
                        pnlAddAlbum.Visible = canEdit;

                        CommandField gvAlbumsButtons = grdAlbums.Columns[0] as CommandField;
                        gvAlbumsButtons.Visible = canEdit;

                        // Profile info
                        editStringFullname.Text = profileRow.FullName;
                        editPelephone.Text = profileRow.Phone;
                        dateBirth.SelectedDate = profileRow.Birthdate;
                        dateStart.SelectedDate = profileRow.StartDate;
                        dateEnd.SelectedDate = profileRow.EndDate;

                        // Read biography:
                        if (File.Exists(jsonBiographyPath))
                        {
                            hiddenJsonBiography.Value = File.ReadAllText(jsonBiographyPath);
                        }
                        else
                        {
                            hiddenJsonBiography.Value = File.ReadAllText(jsonBiographyDefault);
                        }

                        // Load available titles:
                        foreach (Dataset.PeopleTitlesRow title in DbTools.getPeopleTitles())
                        {
                            editListTitle.Items.Add(new ListItem(title.Title, title.TitleID.ToString()));
                        }
                        editListTitle.SelectedValue = profileRow.TitleID.ToString();

                        // Load profile image: TODO --> img has url of itself.
                        loadProfileImage();

                    }
                    else
                    {
                        // Invalid ID:
                        pnlNoID.Visible = true;
                    }
                }
                else
                {
                    // Updates:

                    // If uploaded new profile image file:
                    loadProfileImage();
                }
            }
            else
            {
                // Invalid ID:
                pnlNoID.Visible = true;
            }


        }


        protected void btnTest_Click(object sender, EventArgs e)
        {
            //lblTest.Text = DateTime.Now.ToString();
            //lblTest.Text = PersonalInfoSite.Services.SearchPeople.

        }

        protected void btnEditProfileinfo_Click(object sender, EventArgs e)
        {
            editStringFullname.Edit();
            editListTitle.Edit();
            editPelephone.Edit();
            dateBirth.Edit();
            dateStart.Edit();
            dateEnd.Edit();

            pnlSaveProfile.Visible = true;
            pnlEdit.Visible = false;
        }

        protected void btnSaveProfile_Click(object sender, EventArgs e)
        {
            if (!canEdit) return;

            editStringFullname.Save();
            editListTitle.Save();
            editPelephone.Save();
            dateBirth.Save();
            dateStart.Save();
            dateEnd.Save();

            DbTools.myPagesProfileAdapter.UpdateProfile(
                editStringFullname.Text,
                dateBirth.SelectedDate,
                dateStart.SelectedDate,
                dateEnd.SelectedDate,
                editPelephone.Text,
                int.Parse(editListTitle.SelectedValue),
                pageID
                );

            pnlSaveProfile.Visible = false;
            pnlEdit.Visible = true;
        }

        protected void btnUploadProfilePic_Click(object sender, EventArgs e)
        {
            if (!canEdit) return;

            if (uploadProfilePic.HasFile)
            {
                // Upload image:
                string uploadPath = Server.MapPath(
                    "~/UploadData/Pictures/UploadTemp/" + pageID + "_temp"
                    );
                string savePath = Server.MapPath(
                   "~/UploadData/Pictures/Users/" + pageID + ".png"
                   );
                uploadProfilePic.SaveAs(uploadPath);

                // Resize and convert to PNG:
                resizeImage(savePath, uploadPath, 225, 225);

                File.Delete(uploadPath);

                // Finally upate profile:
                loadProfileImage();
            }
        }


        private void resizeImage(string newFilename, string originalFilename,
                     /* note changed names */
                     int _newWidth, int _newHeight)
        {
            //http://stackoverflow.com/questions/1940581/c-sharp-image-resizing-to-different-size-while-preserving-aspect-ratio

            using (System.Drawing.Image image = System.Drawing.Image.FromFile(originalFilename))
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;

                using (System.Drawing.Image thumbnail =
                    new Bitmap(_newWidth, _newHeight)) // changed parm names
                {
                    using (System.Drawing.Graphics graphic =
                                  System.Drawing.Graphics.FromImage(thumbnail))
                    {
                        graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphic.SmoothingMode = SmoothingMode.HighQuality;
                        graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        graphic.CompositingQuality = CompositingQuality.HighQuality;

                        /* ------------------ new code --------------- */

                        // Figure out the ratio
                        double ratioX = (double)_newWidth / (double)originalWidth;
                        double ratioY = (double)_newHeight / (double)originalHeight;
                        // use whichever multiplier is smaller
                        double ratio = ratioX < ratioY ? ratioX : ratioY;

                        // now we can get the new height and width
                        int newHeight = Convert.ToInt32(originalHeight * ratio);
                        int newWidth = Convert.ToInt32(originalWidth * ratio);

                        // Now calculate the X,Y position of the upper-left corner 
                        // (one of these will always be zero)
                        int posX = Convert.ToInt32((_newWidth - (originalWidth * ratio)) / 2);
                        int posY = Convert.ToInt32((_newHeight - (originalHeight * ratio)) / 2);

                        graphic.Clear(Color.White); // white padding
                        graphic.DrawImage(image, posX, posY, newWidth, newHeight);

                        /* ------------- end new code ---------------- */

                        System.Drawing.Imaging.ImageCodecInfo[] info =
                                         ImageCodecInfo.GetImageEncoders();
                        EncoderParameters encoderParameters;
                        encoderParameters = new EncoderParameters(1);
                        encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality,
                                         100L);
                        thumbnail.Save(newFilename, info[1],
                                         encoderParameters);
                    }
                }
            }
        }

        protected void btnSaveBiography_Click(object sender, EventArgs e)
        {
            if (!canEdit) return;

            // Write json to file:

           

            // Create or open and write json:
            File.WriteAllText(jsonBiographyPath, hiddenJsonBiography.Value);
        }

        protected void btnAddAlbum_Click(object sender, EventArgs e)
        {
            if (txtNewAlbum.Text != "")
            {
                // Add album
                DbTools.myUserAlbums.NewAlbum(pageID, txtNewAlbum.Text, DateTime.Now);

                // Refresh grid:
                grdAlbums.DataBind();
            }
        }

        public int albumPictureCount(object AlbumID)
        {
            if (AlbumID != null && AlbumID is int)
                return DbTools.myAlbumPhotos.GetDataByAlbumID((int)AlbumID).Count;
            return -1; // Error
        }

        protected void grdAlbums_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //https://stackoverflow.com/a/10299099/1997873
            if (e.Row.RowType == DataControlRowType.DataRow) // Can be header row! with no data!
            {
                DataRow row = ((DataRowView)e.Row.DataItem).Row;
                int? id = row.Field<int?>("AlbumID");
               
                // String is a reference type, so you just need to compare with null
                if (id != null) {
                    Label count = (Label)e.Row.FindControl("labelPhotoCount");

                    //https://forums.asp.net/post/5741486.aspx
                    count.Text = albumPictureCount((int)id).ToString();
                }
            }
        }
    }
}