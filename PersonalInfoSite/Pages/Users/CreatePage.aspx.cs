﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PersonalInfoSite.Pages.Users
{
    public partial class CreatePage : UserPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bool PageExists = false;
                int pageID = DbTools.CreatePage(MyUser, out PageExists);

                pnlExistingUser.Visible = PageExists;
                pnlNewUser.Visible = !PageExists;

                pnlAdmin.Visible = DbTools.isAdmin(MyUser);
            }
        }

        protected void btnCreateAdmin_Click(object sender, EventArgs e)
        {
            // Create a new page without owner and go to it.
            int newPageID = DbTools.CreatePageAdmin();
            Response.Redirect("~/Pages/Users/ViewPage?ID=" + newPageID, false);
        }

        protected void btnAdminZone_Click(object sender, EventArgs e)
        {
            // Redirect to admin zone.
            Response.Redirect("~/Pages/Admin/Manage.aspx", false);
        }
    }
}