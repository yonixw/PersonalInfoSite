﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewPage.aspx.cs" MasterPageFile="~/Site.Master" Inherits="PersonalInfoSite.Pages.Users.ViewPage" %>

<%@ Register Src="~/UserControls/EditDatePicker.ascx" TagPrefix="uc1" TagName="EditDatePicker" %>
<%@ Register Src="~/UserControls/EditString.ascx" TagPrefix="uc1" TagName="EditString" %>
<%@ Register Src="~/UserControls/EditDropdownLinked.ascx" TagPrefix="uc1" TagName="EditDropdownLinked" %>




<asp:Content ID="ViewPageContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlValidID" runat="server" Visible="false">

        <div class="half-col">
            <h1>תמונת פרופיל</h1>
            <asp:Image ID="imgMyProfile" runat="server" CssClass="page-profile-pic" />
            <br />

            <asp:Panel ID="pnlUploadProfilePic" runat="server" Visible="false">

                <div style="font-weight: bold; color: orangered" class="page-profile-pic">
                    העלה תמונת פרופיל חדשה:
                <asp:FileUpload ID="uploadProfilePic" runat="server" />
                    <asp:Button ID="btnUploadProfilePic" runat="server" Text="העלה לשרת" OnClick="btnUploadProfilePic_Click" />
                </div>

            </asp:Panel>
        </div>

        <asp:UpdatePanel ID="updateProfile" runat="server">
            <ContentTemplate>
                <div class="half-col">
                    <h1>פרטים אישיים</h1>

                    <asp:Panel ID="pnlEdit" runat="server" Visible="false">
                        <div style="font-weight: bold; color: orangered">
                            ביכולתך לערוך חלק זה
                    <asp:Button ID="btnEditProfileinfo" runat="server" Text="ערוך" OnClick="btnEditProfileinfo_Click" />
                            <br />
                        </div>
                    </asp:Panel>

                    <ul>

                        <li><b>תואר: </b>
                            <uc1:EditDropdownLinked runat="server" ID="editListTitle"
                                ShowEditButton="false" ShowSaveButton="false" />
                        </li>

                        <li><b>שם מלא:</b>
                            <uc1:EditString runat="server" ID="editStringFullname"
                                ShowEditButton="false" ShowSaveButton="false" />
                        </li>

                        <li><b>טלפון/פלאפון:</b>
                            <uc1:EditString runat="server" ID="editPelephone"
                                ShowEditButton="false" ShowSaveButton="false" />
                        </li>

                        <li><b>תאריך לידה:</b>
                            <uc1:EditDatePicker runat="server" ID="dateBirth" CultureName="he-IL"
                                ShowEditButton="false" ShowSaveButton="false" />
                        </li>

                        <li><b>תאריך גיוס:</b>
                            <uc1:EditDatePicker runat="server" ID="dateStart" CultureName="he-IL"
                                ShowEditButton="false" ShowSaveButton="false" />
                        </li>

                        <li><b>תאריך שחרור:</b>
                            <uc1:EditDatePicker runat="server" ID="dateEnd" CultureName="he-IL"
                                ShowEditButton="false" ShowSaveButton="false" />
                        </li>
                    </ul>

                    <asp:Panel ID="pnlSaveProfile" runat="server" Visible="false">
                        <div style="font-weight: bold; color: orangered">
                            לאחר עריכת המידע עליך לשמור:
                    <asp:Button ID="btnSaveProfile" runat="server" Text="שמור" OnClick="btnSaveProfile_Click" />
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>




        <h1>על עצמי (ביוגרפיה)</h1>

        <div id="content-container" dir="ltr">
            <div id="scroll-container">
                <div id="empty-toolbar" style="border: 0px solid #ccc; border-bottom-width: 1px;"></div>
                <div id="editor-container"></div>
            </div>
        </div>

        <script>
            $(document).ready(function ()
            {
                if (!window.quill) // only on the first init (not every postback)
                {
                    var _enable = false;
                    var toolbarOptions = "#empty-toolbar";
                    var _moduls = {};

                <% if (this.canEdit)
            { %>
                    _enable = true; // Let user edit. (front end protection only)

                    toolbarOptions = [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block'],

                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
                    [{ 'direction': 'rtl' }],                         // text direction

                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                    [{ 'font': [] }],
                    [{ 'align': [] }],

                    ['clean']                                         // remove formatting button
                    ];

                <% } %>



                    window.quill = new Quill('#editor-container', {
                        modules: {
                            toolbar: toolbarOptions
                        },
                        placeholder: 'Compose an epic...',
                        scrollingContainer: '#scroll-container',
                        theme: 'snow'
                    });

                    window.quill.enable(_enable);

                    // Set biography content:
                    window.quill.setContents(JSON.parse($(".save-json-biography").val()));
                }
            });

            function updateQuillData()
            {
                $('.save-json-biography').val(JSON.stringify(window.quill.getContents()));
            }
        </script>



        <asp:UpdatePanel ID="pnlBiography" runat="server">
            <ContentTemplate>
                <!-- Place where data is saved: -->
                <input id="hiddenJsonBiography" class="save-json-biography" type="hidden" runat="server" />

                <asp:Button ID="btnSaveBiography" runat="server" Text="שמור" OnClick="btnSaveBiography_Click"
                    OnClientClick="updateQuillData();" />

            </ContentTemplate>
        </asp:UpdatePanel>

        <h1>אלבומים שלי</h1>

        <asp:UpdatePanel ID="pnlMyAlbums" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlAddAlbum" runat="server">
                    <span style="font-weight: bold; color: orangered">צור אלבום חדש:</span>
                    <asp:TextBox ID="txtNewAlbum" runat="server"></asp:TextBox>
                    <asp:Button ID="btnAddAlbum" Text="הוסף" runat="server" OnClick="btnAddAlbum_Click" />
                </asp:Panel>
                <br />
                <asp:GridView ID="grdAlbums" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlDGVAlbums" AllowPaging="True" 
                    ShowFooter="False" ShowHeader="True" CssClass="table-galleries" 
                    DataKeyNames="AlbumID" OnRowDataBound="grdAlbums_RowDataBound">
                    <%--DataKey name info from here: https://stackoverflow.com/questions/21691920/how-to-use-delete-command-in-grid-view-for-updation --%>
                    <Columns>
                        <asp:CommandField ShowDeleteButton="true" ShowEditButton="true" ButtonType="Button"
                            DeleteText="מחיקה" EditText="שנה שם" UpdateText="אישור" CancelText="ביטול"
                            HeaderStyle-CssClass="album-button-edit" />
                        <asp:TemplateField  HeaderText="אלבום" HeaderStyle-CssClass="album-name-th">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkType" runat="server"
                                    Text='<%# Eval("Title")%>'
                                    PostBackUrl='<%# "~/Pages/Users/ViewAlbum.aspx?ID=" + Eval("AlbumID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditName" runat="server" Text='<%# Bind("Title")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="מס. תמונות">
                            <HeaderStyle  CssClass="album-count-th" />
                            <ItemTemplate>
                               <asp:Label runat="server" ID="labelPhotoCount" Text="-1"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlDGVAlbums" runat="server" ConnectionString="<%$ ConnectionStrings:MainDBConnectionString %>" ProviderName="<%$ ConnectionStrings:MainDBConnectionString.ProviderName %>"
                    SelectCommand="SELECT * FROM UsersAlbum WHERE (PageID = ?) ORDER BY DateCreated DESC"
                    UpdateCommand="UPDATE UsersAlbum SET [Title] = ? WHERE ([PageID] = ?) AND (AlbumID = ?)"
                    DeleteCommand="DELETE FROM UsersAlbum WHERE (PageID = ?) AND (AlbumID = ?)">
                    <DeleteParameters>
                        <asp:QueryStringParameter DefaultValue="-1" Name="PageID" QueryStringField="ID" />
                        
                        <%-- SelectedDataKey come from GridView.DataKeyNames --%>
                        <asp:ControlParameter ControlID="grdAlbums" Name="AlbumID" PropertyName="SelectedDataKey" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="-1" Name="PageID" QueryStringField="ID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Title" Type="String" /> <%-- Parameter come from bind (not eval) --%>
                        <asp:QueryStringParameter DefaultValue="-1" Name="PageID" QueryStringField="ID" Type="Int32" />

                        <%-- SelectedDataKey come from GridView.DataKeyNames --%>
                        <asp:ControlParameter ControlID="grdAlbums" Name="AlbumID" PropertyName="SelectedDataKey" />
                    </UpdateParameters>
                </asp:SqlDataSource>
    </ContentTemplate>
        </asp:UpdatePanel>

    </asp:Panel>
    <asp:Panel ID="pnlNoID" runat="server" Visible="false">
        לא נמצא משתמש, אנא חפש משתמש או לחץ "דף אישי" בתפריט למעלה
    </asp:Panel>



    <br />
    Version 3
</asp:Content>
