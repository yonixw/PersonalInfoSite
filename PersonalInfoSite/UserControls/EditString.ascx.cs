﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PersonalInfoSite.UserControls
{
    public partial class EditString : System.Web.UI.UserControl
    {

        public bool ShowEditButton
        {
            get { return btnEdit.Visible; }
            set { btnEdit.Visible = value; }
        }

        public bool ShowSaveButton
        {
            get { return pnlSave.Visible; }
            set { pnlSave.Visible = value; }
        }

        string _myValue;
        public string Text
        {
            get
            {
                return _myValue;
            }

            set
            {
                _myValue = txtEdit.Text = lblValue.Text = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                lblValue.Text = _myValue;
            }


        }


        public void Edit()
        {
            lblValue.Text = Text;

            pnlEdit.Visible = true;
            pnlValue.Visible = false;
        }

        public void Save()
        {
            Text = txtEdit.Text;

            pnlEdit.Visible = false;
            pnlValue.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Edit();
        }
    }
}