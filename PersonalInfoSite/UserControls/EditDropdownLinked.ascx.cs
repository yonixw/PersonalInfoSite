﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PersonalInfoSite.UserControls
{
    public partial class EditDropdownLinked : System.Web.UI.UserControl
    {

        public bool ShowEditButton
        {
            get { return btnEdit.Visible; }
            set { btnEdit.Visible = value; }
        }

        public bool ShowSaveButton
        {
            get { return pnlSave.Visible; }
            set { pnlSave.Visible = value; }
        }

        string _myValue = "-1"; // Index of list but values are in string....
        public string SelectedValue
        {
            get
            {
                return _myValue;
            }

            set
            {
                _myValue = value;
                ddlEdit.SelectedValue = value;
                lblValue.Text = ((ddlEdit.SelectedItem != null) ? ddlEdit.SelectedItem.Text : " ");
            }
        }

        public ListItemCollection Items
        {
            get { return ddlEdit.Items; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                SelectedValue = SelectedValue; // refresh all
            }


        }


        public void Edit()
        {
            pnlEdit.Visible = true;
            pnlValue.Visible = false;
        }

        public void Save()
        {
            SelectedValue = ddlEdit.SelectedValue;

            pnlEdit.Visible = false;
            pnlValue.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Edit();
        }
    }
}