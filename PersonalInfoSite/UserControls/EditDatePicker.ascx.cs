﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PersonalInfoSite.UserControls
{
    public partial class EditDatePicker : System.Web.UI.UserControl
    {
        public const int minYear = 1900;
        public const int maxYear = 2300;

        public bool ShowEditButton
        {
            get { return btnEdit.Visible; }
            set { btnEdit.Visible = value; }
        }

        public bool ShowSaveButton
        {
            get { return btnSave.Visible; }
            set { btnSave.Visible = value; }
        }

        public DateTime SelectedDate
        {
            get
            {
                return dateEdit.SelectedDate;
            }

            set
            {
                if (value.Year < minYear || value.Year >= maxYear)
                    value = new DateTime(minYear, 1, 1);

                dateEdit.SelectedDate = value;
                ddlYear.SelectedIndex = value.Year - minYear; // minYear-minYear = index 0
                ddlMonth.SelectedIndex = value.Month - 1;

                setDays(value.Year, value.Month);
                ddlDay.SelectedIndex = value.Day - 1;

                dateEdit.VisibleDate = dateEdit.SelectedDate;
            }
        }

        private System.Globalization.CultureInfo calanderCulture;
        public string CultureName
        {
            set
            {
                calanderCulture =
                    System.Globalization.CultureInfo.CreateSpecificCulture(value);
            }
        }

        void setDays()
        {
            setDays(
                ddlYear.SelectedIndex + minYear,
                ddlMonth.SelectedIndex + 1);
        }

        void setDays(int year, int month)
        {
            int tempIndex = ddlDay.SelectedIndex;

            ddlDay.Items.Clear();
            for (int i=1;i<=DateTime.DaysInMonth(year,month);i++)
                ddlDay.Items.Add(new ListItem(i.ToString())); ;

            ddlDay.SelectedIndex = Math.Min(tempIndex, ddlDay.Items.Count - 1);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                for (int i = minYear; i < maxYear; i++)
                {
                    ddlYear.Items.Add(new ListItem(i.ToString()));
                }

                foreach (string month in calanderCulture.DateTimeFormat.MonthNames)
                {
                    if (!month.Equals(""))
                        ddlMonth.Items.Add(new ListItem(month));
                }

                for (int i=1;i<=31;i++)
                {
                    ddlDay.Items.Add(new ListItem(i.ToString()));
                }


                SelectedDate = SelectedDate; // Update all controls.

                lblValue.Text = SelectedDate.ToShortDateString();
            }


        }

        void updateEditDate()
        {
            dateEdit.SelectedDate = new DateTime(
                ddlYear.SelectedIndex + minYear,
                ddlMonth.SelectedIndex + 1,
                ddlDay.SelectedIndex + 1
                );
            dateEdit.VisibleDate = dateEdit.SelectedDate;
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            setDays();
            updateEditDate();
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            setDays();
            updateEditDate();
        }

        protected void ddlDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateEditDate();
        }

        protected void dateEdit_SelectionChanged(object sender, EventArgs e)
        {
            SelectedDate = dateEdit.SelectedDate;
        }

        public void Edit()
        {
            lblValue.Visible = false;
            dateEdit.SelectedDate = SelectedDate;

            pnlEdit.Visible = true;
            pnlValue.Visible = false;
        }

        public void Save()
        {
            lblValue.Text = SelectedDate.ToShortDateString();
            lblValue.Visible = true;

            pnlEdit.Visible = false;
            pnlValue.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Edit();
        }
    }
}