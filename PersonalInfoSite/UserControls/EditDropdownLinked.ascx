﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditDropdownLinked.ascx.cs" Inherits="PersonalInfoSite.UserControls.EditDropdownLinked" %>

<asp:Panel ID="pnlValue" runat="server" Visible="true">
    <asp:Label ID="lblValue" runat="server" Text=""></asp:Label>
     <asp:Button ID="btnEdit" runat="server" Text="ערוך" OnClick="btnEdit_Click"  />
</asp:Panel>

<asp:Panel ID="pnlEdit" runat="server" Visible="false">
    <table border="1">
        <tr>
            <td>
                <asp:DropDownList ID="ddlEdit" runat="server" ></asp:DropDownList>         
            </td>
            <td runat="server" id="pnlSave"> 
                <asp:Button ID="btnSave" runat="server" Text="שמור" OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
