﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditDatePicker.ascx.cs" Inherits="PersonalInfoSite.UserControls.EditDatePicker" %>

<asp:Panel ID="pnlValue" runat="server" Visible="true">
    <asp:Label ID="lblValue" runat="server" Text=""></asp:Label>
     <asp:Button ID="btnEdit" runat="server" Text="ערוך" OnClick="btnEdit_Click"  />
</asp:Panel>

<asp:Panel ID="pnlEdit" runat="server" Visible="false">
    <table border="1">
        <tr>
            <td>שנה:
            <asp:DropDownList ID="ddlYear" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            </td>
            <td>חודש:
            <asp:DropDownList ID="ddlMonth" runat="server" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            </td>
            <td>יום:
            <asp:DropDownList ID="ddlDay" runat="server" OnSelectedIndexChanged="ddlDay_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="שמור" OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
    <asp:Calendar ID="dateEdit" runat="server" FirstDayOfWeek="Sunday" ShowGridLines="True" OnSelectionChanged="dateEdit_SelectionChanged">
        <SelectedDayStyle BackColor="#FF9933" BorderStyle="Solid" />
    </asp:Calendar>
</asp:Panel>
