﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PersonalInfoSite
{
    public partial class _Default : Pages.UserPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int pageID = Pages.DbTools.getPageIdFromOwner(MyUser);
            if (pageID > -1)
            {
                // Page Exists! go to it!
                Response.Redirect("~/Pages/Users/ViewPage?ID=" + pageID, false);
            }
            else
            {
                // Need to crete a page!
                Response.Redirect("~/Pages/Users/CreatePage", false);
            }
        }
    }
}