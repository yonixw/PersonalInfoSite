﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using PersonalInfoSite.Pages;
using PersonalInfoSite.DataSets;
using System.Web;
using System.IO;

namespace PersonalInfoSite.Services
{
    
    public class SearchPeople : UserPage
    {
        
        public class SearchResult
        {
            public string FullName;
            public string Title;
            public int PageID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // To support hebrew in IE11 as shown here: http://stackoverflow.com/questions/8076538/encoded-unicode-characters-lost
            // Also IE bug : http://stackoverflow.com/questions/1638499/how-to-get-a-querystring-when-it-is-urlencoded-or-has-percent-characters-in-asp
            string query = Request.Url.Query;
            string searchParam = "";

            int startIndex = query.IndexOf("q=");
            if (startIndex > -1)
            {
                startIndex += "q=".Length;
                int endIndex = query.IndexOf("&", startIndex + 1) ;

                if (startIndex > -1 && endIndex > -1)
                {
                    searchParam = query.Substring(startIndex, (endIndex - startIndex) );
                }
                else if (startIndex > -1 && endIndex < 0)
                {
                    searchParam = query.Substring(startIndex);
                }
            }
           
            

            string toFind_UTF8 = HttpUtility.UrlDecode(searchParam, Encoding.UTF8);
            string toFind_Default = HttpUtility.UrlDecode(searchParam, Encoding.Default);

            List<SearchResult> searchResult = new List<SearchResult>();

            if (toFind_UTF8 != "" && toFind_Default != "")
            {
                searchResult.AddRange(Search(toFind_Default)); // IE (newer) or chrome

                if (toFind_UTF8 != toFind_Default)
                    searchResult.AddRange(Search(toFind_UTF8)); // IE older
            }

            Response.ContentType = "application/javascript";
            Response.Write(DbTools.json.Serialize(searchResult));
            Response.End();
        }



        public List<SearchResult> Search(string find)
        {
            List<SearchResult> result = new List<SearchResult>();

            foreach (Dataset.SearchPeopleRow row in 
                DbTools.mySearchPeopleViewAdapter.GetDataByPartOfName("%" + find + "%"))
            {
                result.Add(new SearchResult()
                {
                    FullName = row.FullName,
                    Title = row.Title,
                    PageID = row.PageID
                });
            }

            return result;
        }

        T readJsonBodyAs<T>()
        {
            //http://stackoverflow.com/questions/3398926/how-to-retrieve-json-via-asp-net-context-request
            var jsonString = String.Empty;

            Request.InputStream.Position = 0;
            using (var inputStream = new StreamReader(Request.InputStream))
            {
                jsonString = inputStream.ReadToEnd();
            }
          
            return (T)DbTools.json.Deserialize(jsonString, typeof(T));
        }
    }
}
