﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PersonalInfoSite.Pages.Users
{
    /// <summary>
    /// Return pictures in site by type and logic name.
    /// </summary>
    public class Picture1 : IHttpHandler
    {
        public enum PictureType
        {
            UserProfile = 1,
            Thumbnailes = 2,
            Fullsize = 3
        }

        public static string pictureService(PictureType picType, string ID)
        {
            return String.Format("~/Services/Picture.ashx?T={0}&ID={1}", 
                ((int)Picture1.PictureType.UserProfile).ToString(),
                ID);
        }

        bool ExistsLocal (HttpContext c, string relativePath)
        {
            return File.Exists(c.Server.MapPath(relativePath));
        }

        class ServerFile
        {
            public static HttpContext context = null;
            string _fullpath;
            string _reltive;

            public ServerFile(string relativePath)
            {
                _reltive = relativePath;
                if (context != null)
                {
                    _fullpath = context.Server.MapPath(_reltive);
                }
            }

            public bool Exist()
            {
                return File.Exists(_fullpath);
            }

            public string RelativePath() { return _reltive; }
            public string FullPath() { return _fullpath; }
        }

        public void ProcessRequest(HttpContext context)
        {
            /* ALL IMAGE ARE PNG. SO WE ONLY GET ID AND RETURN PNG FILE */

            if (context.User.Identity.IsAuthenticated)
            {
                ServerFile.context = context;

                bool requestSuccess = false;
                ServerFile pictureFile = null;

                int picTypeParm = -1;
                string pictureID = null;


                if ((pictureID = context.Request.QueryString["ID"]) != null)
                {

                    if (int.TryParse(context.Request["T"], out picTypeParm))
                    {
                        PictureType picType = (PictureType)picTypeParm;
                        

                        string picureBaseFolder = "~/UploadData/Pictures/";
                        string userProfilePath = picureBaseFolder  + "Users/";
                        string fullsizePicturePath = picureBaseFolder + "Fullsize/";
                        string thumPicturePath = picureBaseFolder + "Thumbnailes/";

                        switch (picType)
                        {
                            case PictureType.UserProfile:
                                ServerFile myProfilePic = new ServerFile(userProfilePath + pictureID + ".png");
                                ServerFile noProfilePic = new ServerFile(userProfilePath + "nopic.png");

                                if (myProfilePic.Exist())
                                {
                                    pictureFile = myProfilePic;
                                    requestSuccess = true;
                                }
                                else
                                {
                                    pictureFile = noProfilePic;
                                    requestSuccess = true;
                                }
                                break;

                            case PictureType.Fullsize:
                                ServerFile fullsizePic = new ServerFile(fullsizePicturePath + pictureID + ".png");
                                if (fullsizePic.Exist()) {
                                    pictureFile = fullsizePic;
                                    requestSuccess = true;
                                }
                                break;

                            case PictureType.Thumbnailes:
                                ServerFile thumbPic = new ServerFile(thumPicturePath + pictureID + ".png");
                                if (thumbPic.Exist())
                                {
                                    pictureFile = thumbPic;
                                    requestSuccess = true;
                                }
                                break;
                        }
                    }
                }

                if (!requestSuccess)
                {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/javascript";
                    context.Response.Write("{\"error\" : \"Error proccesing image. Excepting type (T) and ID (ID).\"}");
                }
                else
                {
                    context.Response.ContentType = "image/png";
                    context.Response.WriteFile(pictureFile.RelativePath()); // WriteFile support Relative path
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}